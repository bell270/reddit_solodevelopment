using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : MonoBehaviour
{
    public Vector2 position;
    private int growthLimit = 6;
    public int growthCurrent = 0;
    public GameObject Root;
    public GameObject Leaf;
    public GameObject Stem;
    public GameObject Leaf2;
    public GameObject Fruit;
    public ParticleSystem FruitParts;

    public string SoundSource;

    public bool isBusy = false;

    public Animator animator;

    void Start()
    {
        //transform.position = position;
        animator.SetInteger("Growth", growthCurrent);
    }
    
    public void Grow(int clicks)
    {
        StartCoroutine(GrowAnimation(clicks));
    }

    private IEnumerator GrowAnimation(int clicks)
    {
        while (clicks > 0)
        {
            growthCurrent = (growthCurrent + 1) % growthLimit;
            clicks--;
            animator.SetInteger("Growth", growthCurrent);
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void SetGrowth(int level)
    {
        growthCurrent = level;
        animator.SetInteger("Growth", growthCurrent);
    }

    public void CollectFruit()
    {
        // Play sound
        FruitParts.Play();
        FindObjectOfType<Player>().CollectFruit();

        Color fruitColor = Color.HSVToRGB(Random.Range(0f, 1f), 0.7f, 1f);
        Fruit.GetComponent<SpriteRenderer>().color = fruitColor;
        ParticleSystem.MainModule main = FruitParts.main;
        main.startColor = fruitColor;
    }

    public void AnimationStart()
    {
        isBusy = true;
        FindObjectOfType<AudioManager>().PlayClip(SoundSource, (0.5f + 0.1f * growthCurrent));
    }

    public void AnimationEnd()
    {
        isBusy = false;
    }
    private bool EOAnimation()
    {
        return isBusy;
    }
}
