using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool isPaused = false;
    public List<Flower> flowers;
    public List<Vector2> spawns;
    public List<Buff> buffs;
    public List<GameObject> powerUps;

    public int fruits;
    private int fruitCost;
    private int totalFruits;

    public int clickPower;
    public float autoClickPower;
    private float autoClickAccum;
    private float autoClickTimer;
    public int clickCounterUsed;
    public int clickCounterTotal;

    public GameObject FlowerPrefab;
    public TextMeshProUGUI FruitsCounter;

    public ParticleSystem Rain;

    void Start()
    {
        LoadPlayerPref();
    }

    // Update is called once per frame
    void Update()
    {
        CheckPowerUps();
        AutoClick();
        if (!isPaused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickCounterTotal++;
                foreach (Flower flower in flowers)
                {
                    if (!flower.isBusy)
                    {
                        clickCounterUsed++;
                        flower.isBusy = true;
                        flower.Grow(clickPower);
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                isPaused = false;
                Time.timeScale = 1;
                _ = SceneManager.UnloadSceneAsync(1);
            }
            else
            {
                isPaused = true;
                Time.timeScale = 0;
                PlayerPrefs.SetString("ft", totalFruits.ToString());
                PlayerPrefs.SetString("fu", (totalFruits - fruits).ToString());
                PlayerPrefs.SetString("ct", clickCounterTotal.ToString());
                PlayerPrefs.SetString("cu", clickCounterUsed.ToString());
                PlayerPrefs.SetString("p0l", buffs[0].level.ToString());
                PlayerPrefs.SetString("p0c", (1 * (buffs[0].level > 0 ? 1 : 0) * Mathf.Pow(2, (buffs[0].level-1))).ToString());
                PlayerPrefs.SetString("p1l", buffs[1].level.ToString());
                PlayerPrefs.SetString("p1c", (5 * (buffs[1].level > 0 ? 1 : 0) * Mathf.Pow(2, (buffs[1].level - 1))).ToString());
                PlayerPrefs.SetString("p2l", buffs[2].level.ToString());
                PlayerPrefs.SetString("p2c", (25 * (buffs[2].level > 0 ? 1 : 0) * Mathf.Pow(2, (buffs[2].level - 1))).ToString());
                SceneManager.LoadScene(1, LoadSceneMode.Additive);
            }

        }
    }

    private void FixedUpdate()
    {
    }

    private void CheckPowerUps()
    {
        int i = 0;
        foreach (GameObject PowerUp0 in powerUps)
        {
            foreach (Transform child in PowerUp0.transform)
            {
                if (child.tag == "PowerUpButton")
                {
                    child.GetComponent<Button>().interactable = buffs[i].IsAvailible(fruits);
                }
                if (child.tag == "PowerUpCost")
                {
                    string costString = "-" + buffs[i].cost;
                    if (buffs[0].level >= buffs[i].levelMax) costString = "MAX";
                    child.GetComponent<TextMeshProUGUI>().text = costString;
                }
            }
            i++;
        }
    }

    public void LoadPlayerPref()
    {
        clickPower = 1;
        autoClickPower = 0;
        fruitCost = 1;
        flowers = new List<Flower>();
        ChangeFruitsBank(fruitCost);
        clickCounterUsed = 0;
        for(int i = 0; i < 3; i++)
        {
            int cost = 0;
            int levelMax = 0;
            switch (i)
            {
                case(0):
                    cost = 1;
                    levelMax = spawns.Count;
                    break;
                case (1):
                    cost = 5;
                    levelMax = 11;
                    break;
                case (2):
                    cost = 25;
                    levelMax = 24;
                    break;
                default:
                    cost = 99999;
                    break;
            }
            buffs.Add(new Buff(i, 0, cost, levelMax));
        }
        CheckPowerUps();
    }

    public void SavePlayerPref()
    {

    }

    public void AddFlower()
    {
        if (spawns.Count > 0)
        {
            int idx = Random.Range(0, flowers.Count == 0 ? 0 : spawns.Count);
            Flower flower = Instantiate(FlowerPrefab, spawns[idx], Quaternion.identity).GetComponent<Flower>();
            flower.transform.SetParent(transform);
            flower.position = spawns[idx];
            flowers.Add(flower);
            flower.SoundSource = "bloom" + flowers.Count;
            spawns.RemoveAt(idx);
            ChangeFruitsBank(-buffs[0].cost);
            buffs[0].LevelUp();
        }
    }

    public void AddClickPower()
    {
        ChangeFruitsBank(-buffs[1].cost);
        buffs[1].LevelUp();
        clickPower = 1 + buffs[1].level;
        fruitCost = buffs[1].level;
    }

    public void AddAutoClick()
    {
        ChangeFruitsBank(-buffs[2].cost);
        buffs[2].LevelUp();
        autoClickPower = buffs[2].level;
    }

    public void UnlagFlowers()
    {
        Rain.Play();
        foreach(Flower flower in flowers)
        {
            flower.isBusy = false;
        }
    }

    public void CollectFruit()
    {
        ChangeFruitsBank(fruitCost);
    }

    public void ChangeFruitsBank(int diff)
    {
        fruits += diff;
        if (diff > 0)
        {
            totalFruits += diff;
        }
        FruitsCounter.text = fruits.ToString();
    }

    public void SpeedUp(float scale)
    {
        switch (scale)
        {
            case(2f):
                Time.timeScale = scale;
                break;
            default:
                Time.timeScale = 1;
                break;
        }
    }

    public void AutoClick()
    {
        autoClickTimer += Time.deltaTime;
        if(autoClickTimer >= 1)
        {
            autoClickAccum += autoClickPower;
            if (autoClickAccum > 0 && (autoClickAccum - (int)autoClickAccum) == 0)
            {
                foreach (Flower flower in flowers)
                {
                    if (!flower.isBusy)
                    {
                        flower.isBusy = true;
                        flower.Grow((int)autoClickAccum);
                    }
                }
                autoClickAccum = 0f;
            }
            autoClickTimer = 0f;
        }
    }
}
