using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff
{
    public int type;
    // 0 = flower count
    // 1 = bigger click
    // 2 = auto click
    // 3 = ?

    public int level;
    public int levelMax;
    public int cost;

    public Buff(int type, int level, int cost)
    {
        this.type = type;
        this.level = level;
        this.cost = cost;
        levelMax = -1;
    }

    public Buff(int type, int level, int cost, int levelMax)
    {
        this.type = type;
        this.level = level;
        this.cost = cost;
        this.levelMax = levelMax;
    }

    public bool IsAvailible(int bank)
    {
        return (level < levelMax && bank >= cost);
    }

    public int LevelUp()
    {
        int oldCost = cost;
        level++;
        cost *= 2;
        return oldCost;
    }
}
