using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject OptionsMenu;

    public TextMeshProUGUI fruitTotal;
    public TextMeshProUGUI fruitUsed;
    public TextMeshProUGUI ClickTotal;
    public TextMeshProUGUI ClickUsed;
    public TextMeshProUGUI pu0l;
    public TextMeshProUGUI pu0c;
    public TextMeshProUGUI pu1l;
    public TextMeshProUGUI pu1c;
    public TextMeshProUGUI pu2l;
    public TextMeshProUGUI pu2c;


    private void Awake()
    {
        OptionsMenu.SetActive(false);
        Stats();
    }

    public void ResumeGame()
    {
        Player player = FindObjectOfType<Player>();
        if(player != null) player.isPaused = false;
        Time.timeScale = 1;
        SceneManager.UnloadSceneAsync(1);
    }

    public void GoToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    private void Stats()
    {
        fruitTotal.text = PlayerPrefs.GetString("ft", "");
        fruitUsed.text = PlayerPrefs.GetString("fu", "");
        ClickTotal.text = PlayerPrefs.GetString("ct", "");
        ClickUsed.text = PlayerPrefs.GetString("cu", "");
        pu0l.text = PlayerPrefs.GetString("p0l", "");
        pu0c.text = PlayerPrefs.GetString("p0c", "");
        pu1l.text = PlayerPrefs.GetString("p1l", "");
        pu1c.text = PlayerPrefs.GetString("p1c", "");
        pu2l.text = PlayerPrefs.GetString("p2l", "");
        pu2c.text = PlayerPrefs.GetString("p2c", "");
    }
}
